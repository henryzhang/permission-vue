const users = {
  'admin-token': {
    roles: ['admin'],
    introduction: 'I am a super administrator',
    avatar: 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
    name: 'Super Admin'
  },
  'editor-token': {
    roles: ['editor'],
    introduction: 'I am an editor',
    avatar: 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
    name: 'Normal Editor'
  }
}

export default [
  // 获取验证码
  {
    url: '/platform/sysMain/images/captcha/v1',
    type: 'post',
    response: config => {
      return {
        'data': {
          'verUUidCode': '98e79f7b0fb84aed8bcdd3b172b9e688',
          'baseImg': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIIAAAAwCAIAAABSYzXUAAAP0UlEQVR42u1aC3BTZRbGdV3HUdd1\\r\\nx3EcZ2fHddZ9zI4z65SCIAiKsCDL7IIP0HURx7UriygvQRBYFAV1RQR53aZpS9NHahvbkjZtmj5o\\r\\n+n6koZT0lZY2fYXQpq+0xJaG7Hdzbv/e3CSt6Lo6u/nn9M79H/fm3vP953znnNsZ7mD7HrQZQRUE\\r\\nYQi2IAxBGIItCEMQhmALwhCEIdiCMARhCDbfNu5y6S2W9sHBIAzfQRsaHc1oatqu0y06fTqE47iq\\r\\nqv9TGDrrOjOPZZ7ecpoL46I2RWmOaNovtH/bP4pd/1l5+V+/+GKWTAbtk6yIj0+8cOGbwmC7aDuf\\r\\ncz43IvfMv84otivsHfbvj67LbMOHz9uez7346JmGexU1NfYrGHS5XEUJRfE74/HYQ71DGBkZHDEV\\r\\nmBJ2JxTEFIx9OeZ7nzHXWHF7sbpRvb9g/+aszQuiFsyVz92o2dg52DntM/SOjGibm/fl5y+LjWWq\\r\\nh7yclhZTU2O6fBl+6fq44dq1a66rk9c4eh3K3crwV8NV+1U5shy9Qo/z1A9TvxONu665R/E30doc\\r\\now8oa28MNzykqluTczFM34bzuakNmAIG6YfTR0dGJXfACOwDSLiuuc5Zz2WZsw4WHtym3TY/cn4I\\r\\nFxIaHrpSuXKHbsepylMvp72MkTkRcxbHLG7tb/Xr8au6uk5UVtLG/ye3rp+7bZT74QHFtv0FBTkt\\r\\nLf1Op5936Mpz9xr9w2CpsZQmlWqOaiJfj4T9QmK2xnSYOjClel+FbtaxLLaYbNzR55hCXyNjY0sU\\r\\nCmyH2TLZnIiIeXL5SqUy9+JFkUKvYQ3Ed5tImtoysLW0Y5nGfHukcQZXBbk75py2g6e7EFUduiuy\\r\\nzGzxXafPYaTCaIEd+GJATVuvXXJsSSgXCi0vVSwFBodKDmmbtabLJmDDlqU1pGEBrAHH9enr2Xiz\\r\\n3Z5sMm3VahdGRdGWn8Md00XMdXMzHIpf4OiOvN09aPbzw85et/ZP/AKI7hkpDIZ0A9SauCfRqDE2\\r\\nVzZbm61wqRgBJPVF9YRK0jtJbH3mZ5kYMVeYp9AdLJRMknaNurER3blyeUtfHy3AfmGWC6gWx8Rs\\r\\n1GiOlpdL7vOuoRtq/XXihQNGq7K5r8jq0HUOYgSQRNT3ECoPJpnY+uWZZozsOJ4HXxTo2RIvJEKz\\r\\nG05sgBea4hWa7XiFkNcyXoNrmiefd6axEc9Me4sEFrAuJUVZmu5M+C2vWah43Oku3sifp8yS3u5y\\r\\nhTv2Z8KU6iGceMHQ3dhNitae1IrH0z5Kw2DEPyJoVr5RzqbAEBgpjCuc4h0OFhaKY4NIo3GBZ+88\\r\\nnZhII6sSoQuuvLPTfuVKcXv78yoVuqHh4bAPdpOz3UOk6JXaZvHN56U1YPDmCAPN3iqvZlNgCIw8\\r\\n/mE28YHfhl0P/T596Gm/s3U9dTAOeKRZstlYNiGzmPYR9uzJy8M+432Orcwdczev3Mq9E67K6Y6/\\r\\njx8xHZ+8qTneHXEzP5i1gl/gaHefvssLhoRdCaTo6E3RXm6q1kLjYAI6AWfQVGVqpS9skvZGZiae\\r\\nuLKri7o7c3KWKhRwTdj45JGgcSwYm/BIMAV6yYK2NnaT+xNqSdF3RhvFN8+wDNA4mIBOGF/sruxC\\r\\n96EPcqZ2dGCC0FOhfU7eNKut1aUdpeRIjVbr/MjHRNoXZCY3C08Isj1vs3l5UeUDvHIzl3u7+Ax+\\r\\nEHufXJzhXcERFa4XrVF7wSDfII97Kw4CzV5uvexlvHsSmSng2Nct+BNzmRldON8p3nOuR+OuCeRg\\r\\nvIABQRtsgtwrND4/MpKtxxQEg2A8NniLvPrncechvLu/PCy+P9wUMwUcTX0CGcaa7ejedaxkCmtA\\r\\n25G1A8rdmbPrqcRnSdFPxm4Bh3m2wvMh3CMh3DwanxPxyLbs3Q09Df5vJLtJUHGnzmtc9ww/aI51\\r\\n56wRFpw/7LXAWiS1BngeMDA022r0igdM+SaCARrHETTO+Jz8VaCXtAwM4H1eSp2MpmAHAGZ5XNxz\\r\\nycnAhpgD+4stgAr25udjcJtWK7YGeB4wMDSb0tov/onjpssEw33xPEigccbn6N7EVfrlBuRTiB1T\\r\\n6uu3JEXN5B723vIzH444DDLbpj2F7vL4FTiq6lRi0vbTwm8UtKy4x+0UbeKRbp6oaSr6Tt4+vDDQ\\r\\nu6Pu8IKBAFB/osaxqbRJPHV19CrZAYVPbHbg0gDBI45rffn5fb2ejWzOynosOhrbf7Yno6EjGG8i\\r\\nYHehSzQOzztpIh4AHlc34hjT1OsViV11kR1Q+MRmGwecBE/0W3G9Aw5S+qelpZsyMxHRi/OpEO5A\\r\\nCPcEdD0/cvEf43ibOFlxEncYHh1m2CSbkqeJoFUhvKIJDM0yr6n6CIG3Hd4JY4fWLb9FStH5kflQ\\r\\nKDIyHBGzSn6lOLEY4xShItZmiQXBIHFiEn7WtbQEimXByRqzubFX0B26xBMUTbGVa/NboVBkZDgi\\r\\nZpXST3E77388EepqXRNC+IympqNl5QRDaHhCKBcuzqQotlmVoFx7SvF+ZCq8PJ4ExDDuGke0CqU/\\r\\nl/wc3Xl10mqC4c3sN6eBAW4HuvaolZfaI95pjk/E3BjNDGiGb7SKBA1HQCIsLml0DvHett/az+Il\\r\\n5G7sKrIP5qYkLUytxjt3eleypmjI8uGUwOe4Cl5LEq0iQfMo2gxGga63F9UfrTR+Vl4elpmP8R+E\\r\\nV+L4U07HdH0jV4GRX3LK2Vz4H45FrFd+8am+GAZ6wdJVX97kN4tmFjAyNoLuh0UfUheh6vRPr105\\r\\naRAIhwYaA66sOSSg5QsDEgUWDrHgR7ZeBj74cuRLnCNhpr0Px8WuokFc6/fnKF9j3Sa7vchiwZaH\\r\\nontGRlikMTo+3jU0BHk1Pf3PCQkE3l+Sk8HS8FcIrhZ9ngOF3uDZ3T/hckjLN3CVP+KKf8/JcX4b\\r\\nV0B7/64IPaKA7Todrv1NAu+mjpxrxw9RTSlqUxTVlJBXB6opvaB6AXrXW3hHmnsxl/klxK/TwDDm\\r\\ncCc9OKnf6vf8LzMeEGMghaGroYu0DFHtV9EgBU60/ev0db4RreYEzyhluefgfLFDIcjdkeBAwg0G\\r\\nyg+wYffl50MmIpDrll9x8aRlngNkRYAKuv6xnN/ss5IMcPq7y1p8I9pndPxgZEPvdZVJjpYfhdKj\\r\\njdEetznCYMhoypj+YmuRoFzoOmAx4HEvGFIe9oIB5slgOLVBRoo79G4sjWyNSn42XnnSYysnXp10\\r\\ntZt38bOvv819Pf0yAW0u9dTCwN408n5hIZK+ZJMJjv5sW8ckDJGCop/KFlSf1zUEor7FQ9S3iDK4\\r\\nvxW0YWRDkeW6YCAL2Jmzk7rgCYJhX/6+6TDQ8xkcpCtvqmXOXiGtg5RsRkohLWZwEwkaZKZHFy/u\\r\\nF7rvbea7G/Z4QPr7pPpe+Yjnht175XDlcCYQhCIE4ZrkZCzYnp0dYeA37Nu5eD3uk5KSmeFehImV\\r\\njBjQfcGTRS9RKCTPdpPMwJCgHO1IrY26CCfRBXVTHjf5OnV8kWNdfut1wWAZsIhZ+p2z7xAM4mqS\\r\\nv4qHkk8dzjzqvnJp+t8YbOGD1+yn/FdYoz1hEklKVQ0ci6bkHBupNjS1XuqJ3RUf+1Ycu6SlqgVT\\r\\nOk7n+1tvZmdTlULcrbXZeBvKysLJqx4OSGsQEqIdOp5dyXEBM8ndKC0gaXfwgYexd4SNnO0e6nVe\\r\\nBYdjGbvk85Y+TME1BS7TugzdBklCgHhJzMkas4ZgWBC1IHCYFM9v7ap914E2TAewIb7yhYFSBxJL\\r\\nrYXFQoptPDzqj9V+vmxcaMeU5qjGb/4MhVJpCJkaurCY3IsXWQhLaRpIe4IbVcxEGDaS1IEkYyJH\\r\\ng4O6xxPFLlT7CUsy2/na3zKN/8ojUoFlscug37Upa2EB4ikqawsBZLdhIoueE1CnFKca3vUzheQg\\r\\nda5bcS8/O+5d7m6K4ZHoypPCQKkDCQt+wM+8cXyQgmNPW4/kkuH+YT7b2Mb7EIfR2LJ9e82SJVUh\\r\\nIcaFC5cdO8bHnUlJR8vLY2tqcP5xcTEEsRPFSMhUMTgwUYsXE/glh7R4TqkDibJZqKasyeHrdw+n\\r\\n1ONY1TMi/eI2PIZx4OSrnNiaWNrgLB5NqE0QXPdVJ0ZWJa6aiOJGac1s2eyAMCTcL/h6uCZGAIhK\\r\\nqdDEBF2798NU7oZ38l/oJqlIqaDB2txadBHhIZb1dT4ul4uvNW2QW6OiAIAtLm7UZuOfvqvrPVEp\\r\\nmAR4gDzeOXuWmQtUT+eIXxk/r4j3U6Si1IFkV4Xg6IgenkhvAiX4Op+rrmsS0hbeyFZLirZfsW/R\\r\\nbmGxELy/bdgmr4YRh2SZs8TGAYHpBIQhf52QK2B3m066816YLDExkd8qrIERTJa1z0oDVpY6kLAM\\r\\nrtXYii4SCPq6MHBpQErsxBxLnxydKKMKqV9vb6g3GyMcmi2TVVutbPsviIpKqK3Nb209VFKCBTEe\\r\\no2G1DXGDBTAY1k6wbkprP184khno60LjgPQjF613jHl5fwAAtRZZiqiLFCFMHRYaHgp5LPoxJMzF\\r\\n7cVi/iAY9uTtCUzral7FUXdIVS8RxT3CSfEb4qK3FAZW0xZ/ZbN32GkEpoCjXqEfHxtH2lyWXCb+\\r\\nSHdJEef7eH1OJ7S8Ljw8dAKJ5XGTy5CXScyFquLiErdvTVv8la3GfoVGKEUI07c5x11qy8CbZV4f\\r\\n6cSfSz2ctJdlBtM2VZ2KYGjsbZxqXebyyRRaLJ//zt2WxluAgMS9k+N+0zc+jrINMhjkG+XgZKro\\r\\nsayNnUsk7sUD5Iv8NlhJ9RNP6C0WmcGQWl8vKfyJBbaCUJVVxb2CkcEvGQy3yqvByVTRY1kbO5cI\\r\\nmENyK/gi7Pr5kfMvOaYPLl9KfQkYHCk7Mt1/IDnd6oVeAIAwxP4HzkqCRCAYyNH7lcK4wkBTkNzH\\r\\nV039kCBtv1/SYSt78vLWp6eDFeDBViqVld6eTeLo/cr6QkugKUhaW7/v3aqt1YCB6CHQM9f11L1y\\r\\n5hUw81e0Gx6JnDW8n8lawXO1b2Hcu5QkiOTrm+CCPaU9JqAEuB34H+ewUyHKKgSL2SBX7VfV6etA\\r\\nzqMB1EfWgMDpm/83BpX2mIAS4Hbgf5AxUPFVLGDmh1R1yOCm+MKMHG3R6UUxNTFgZqIBDOpadAcL\\r\\nD65OWo046qPij77KP8VcR2Mf4IRKxiy3o90PDMYsI1QP5Qaq1vltiFN7z5wJNNvFcc1bt37zV/jA\\r\\naIXqoVwWsH7zVtBWQP8UA35G2IpwCN3jFcfLO8un+c7ztdv5w7zFAIA67uv/u5jfZs/IaAwL828K\\r\\nNtu5RYuuNDe7gy1A+4/BcG183PTsswDDF4PzK1ZYPZ+dg+1bh4FPG1tbsevhf3BCfIBzjAQx+K/C\\r\\nwEcKQ0OWAwdYMcO8aVPQF30HMARbEIYgDMEWhCEIQ7AFYfgfav8GoReCghr9WSAAAAAASUVORK5C\\r\\nYII=',
          'imgType': 'data:image/png;base64,'
        },
        'msg': 'OK',
        'oK': true,
        'ok': '',
        'status': 200
      }
    }
  },
  // 用户退出
  {
    url: '/platform/sysUser/logout/v1',
    type: 'post',
    response: config => {
      return {
        'data': '安全退出成功',
        'msg': 'OK',
        'oK': true,
        'ok': '',
        'status': 200
      }
    }
  },
  // 密码登录
  {
    url: '/platform/sysMain/login/v1',
    type: 'post',
    response: config => {
      return {
        data: '登录成功',
        msg: '5cc9a0b69dfa4db6b0f87c9e45f5713b',
        oK: true,
        status: 200
      }
    }
  },

  // 获取菜单权限
  {
    url: '/platform/authority/dynamicMenu/v1',
    type: 'post',
    response: config => {
      return {
        'data': {
          'acls': [
            {
              'checked': true,
              'children': [
                {
                  'checked': true,
                  'children': [

                  ],
                  'createBy': '',
                  'createTime': '2019-08-02 15:11:28',
                  'defaultexpandedKeys': [

                  ],
                  'delFlag': 0,
                  'disabled': false,
                  'disabledFlag': 0,
                  'hasAcl': true,
                  'id': 195332810922397703,
                  'idStr': '195332810922397703',
                  'label': '首页',
                  'remark': '',
                  'sysAclAction': 'index',
                  'sysAclCode': '',
                  'sysAclComponentName': '',
                  'sysAclIcon': '',
                  'sysAclLevel': '0.195332810922397711',
                  'sysAclName': '首页',
                  'sysAclParentId': 195332810922397711,
                  'sysAclParentIdStr': '195332810922397711',
                  'sysAclPermissionCode': '1',
                  'sysAclRouter': '',
                  'sysAclSeq': 1,
                  'sysAclType': 1,
                  'sysAclUrl': '188696496605106236',
                  'updateBy': '李杰',
                  'updateTime': '2019-08-28 14:09:55',
                  'uuid': '188696496605106236'
                },
                {
                  'checked': true,
                  'children': [

                  ],
                  'createBy': '',
                  'createTime': '2019-08-02 15:11:02',
                  'defaultexpandedKeys': [

                  ],
                  'delFlag': 0,
                  'disabled': false,
                  'disabledFlag': 0,
                  'hasAcl': true,
                  'id': 195332810922397704,
                  'idStr': '195332810922397704',
                  'label': '产品提优',
                  'remark': '',
                  'sysAclAction': '',
                  'sysAclCode': '',
                  'sysAclComponentName': '',
                  'sysAclIcon': '',
                  'sysAclLevel': '0.195332810922397711',
                  'sysAclName': '产品提优',
                  'sysAclParentId': 195332810922397711,
                  'sysAclParentIdStr': '195332810922397711',
                  'sysAclPermissionCode': '1',
                  'sysAclRouter': '',
                  'sysAclSeq': 1,
                  'sysAclType': 1,
                  'sysAclUrl': '188696496605106237',
                  'updateBy': '李杰',
                  'updateTime': '2019-08-28 14:10:02',
                  'uuid': '38b8f82768e9407aa0abea4b83f3a0b0'
                },
                {
                  'checked': true,
                  'children': [
                    {
                      'checked': true,
                      'children': [
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '',
                          'createTime': '2019-04-15 11:09:36',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195332810909814787,
                          'idStr': '195332810909814787',
                          'label': '企业列表',
                          'remark': '',
                          'sysAclAction': 'list',
                          'sysAclCode': '',
                          'sysAclComponentName': 'SysEnterpriseList',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705',
                          'sysAclName': '企业列表',
                          'sysAclParentId': 195332810922397705,
                          'sysAclParentIdStr': '195332810922397705',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'SysEnterpriseList',
                          'sysAclSeq': 1,
                          'sysAclType': 1,
                          'sysAclUrl': '/api/platform/enterprise/sysUserGroupPage/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 11:42:40',
                          'uuid': '1b8ac96d955e4e78b8e0820b6e2fd890'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 10:22:09',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195872957032173568,
                          'idStr': '195872957032173568',
                          'label': '企业详情',
                          'remark': '',
                          'sysAclAction': 'detail',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705',
                          'sysAclName': '企业详情',
                          'sysAclParentId': 195332810922397705,
                          'sysAclParentIdStr': '195332810922397705',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'SysEnterpriseList',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/enterprise/detail/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 11:42:54',
                          'uuid': 'fecaff51af264f5aa32fce6dc2adfaba'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 10:23:35',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195873316945399808,
                          'idStr': '195873316945399808',
                          'label': '新增',
                          'remark': '',
                          'sysAclAction': 'add',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705',
                          'sysAclName': '新增',
                          'sysAclParentId': 195332810922397705,
                          'sysAclParentIdStr': '195332810922397705',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'SysEnterpriseList',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/enterprise/save/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 11:43:00',
                          'uuid': 'dc33944794704f0e94236edb2dff33e6'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 10:23:50',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195873379507638272,
                          'idStr': '195873379507638272',
                          'label': '编辑',
                          'remark': '',
                          'sysAclAction': 'edit',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705',
                          'sysAclName': '编辑',
                          'sysAclParentId': 195332810922397705,
                          'sysAclParentIdStr': '195332810922397705',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'SysEnterpriseList',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/enterprise/modify/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 11:43:06',
                          'uuid': 'd279c68cb9484eb8bcbb36e675b2f452'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 10:25:05',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195873691517718528,
                          'idStr': '195873691517718528',
                          'label': '为企业授权(授权)',
                          'remark': '',
                          'sysAclAction': 'aclsToVip',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705',
                          'sysAclName': '为企业授权(授权)',
                          'sysAclParentId': 195332810922397705,
                          'sysAclParentIdStr': '195332810922397705',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'SysEnterpriseList',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/enterprise/aclsToEnterprise/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 11:43:12',
                          'uuid': '83dc22991ed14e8b8210118ca5bdbb4f'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 10:25:38',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195873832010125312,
                          'idStr': '195873832010125312',
                          'label': '为企业授权(回显)',
                          'remark': '',
                          'sysAclAction': 'aclDetail',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705',
                          'sysAclName': '为企业授权(回显)',
                          'sysAclParentId': 195332810922397705,
                          'sysAclParentIdStr': '195332810922397705',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'SysEnterpriseList',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/enterprise/aclDetail/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 11:43:26',
                          'uuid': '821540d6a1d04b969ab7b6d6d5dce8f0'
                        },
                        {
                          'checked': true,
                          'children': [
                            {
                              'checked': true,
                              'children': [

                              ],
                              'createBy': '李杰',
                              'createTime': '2019-08-28 11:13:07',
                              'defaultexpandedKeys': [

                              ],
                              'delFlag': 0,
                              'disabled': false,
                              'disabledFlag': 0,
                              'hasAcl': true,
                              'id': 198060110281576448,
                              'idStr': '198060110281576448',
                              'label': '企业用户列表',
                              'remark': '',
                              'sysAclAction': 'userList',
                              'sysAclCode': '',
                              'sysAclComponentName': '',
                              'sysAclIcon': '',
                              'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705.198058969976147968',
                              'sysAclName': '企业用户列表',
                              'sysAclParentId': 198058969976147968,
                              'sysAclParentIdStr': '198058969976147968',
                              'sysAclPermissionCode': '1',
                              'sysAclRouter': 'SysEnterpriseList',
                              'sysAclSeq': 1,
                              'sysAclType': 2,
                              'sysAclUrl': '/api/platform/enterprise/userList/v1',
                              'updateBy': '李杰',
                              'updateTime': '2019-08-28 11:30:16',
                              'uuid': 'a0dcd785acf74c19838299f1fad042fa'
                            },
                            {
                              'checked': true,
                              'children': [

                              ],
                              'createBy': '李杰',
                              'createTime': '2019-08-28 11:24:33',
                              'defaultexpandedKeys': [

                              ],
                              'delFlag': 0,
                              'disabled': false,
                              'disabledFlag': 0,
                              'hasAcl': true,
                              'id': 198062985040760832,
                              'idStr': '198062985040760832',
                              'label': '企业用户新增',
                              'remark': '',
                              'sysAclAction': 'add',
                              'sysAclCode': '',
                              'sysAclComponentName': '',
                              'sysAclIcon': '',
                              'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705.198058969976147968',
                              'sysAclName': '企业用户新增',
                              'sysAclParentId': 198058969976147968,
                              'sysAclParentIdStr': '198058969976147968',
                              'sysAclPermissionCode': '1',
                              'sysAclRouter': 'SysEnterpriseUserList',
                              'sysAclSeq': 1,
                              'sysAclType': 2,
                              'sysAclUrl': '/api/platform/enterprise/userAdd/v1',
                              'updateBy': '李杰',
                              'updateTime': '2019-08-28 11:31:08',
                              'uuid': 'c87fd3a3c2e941c2b7a50db3a9cd0e4c'
                            },
                            {
                              'checked': true,
                              'children': [

                              ],
                              'createBy': '李杰',
                              'createTime': '2019-08-28 11:25:08',
                              'defaultexpandedKeys': [

                              ],
                              'delFlag': 0,
                              'disabled': false,
                              'disabledFlag': 0,
                              'hasAcl': true,
                              'id': 198063131954647040,
                              'idStr': '198063131954647040',
                              'label': '企业用户编辑',
                              'remark': '',
                              'sysAclAction': 'edit',
                              'sysAclCode': '',
                              'sysAclComponentName': '',
                              'sysAclIcon': '',
                              'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705.198058969976147968',
                              'sysAclName': '企业用户编辑',
                              'sysAclParentId': 198058969976147968,
                              'sysAclParentIdStr': '198058969976147968',
                              'sysAclPermissionCode': '1',
                              'sysAclRouter': 'SysEnterpriseUserList',
                              'sysAclSeq': 1,
                              'sysAclType': 2,
                              'sysAclUrl': '/api/platform/enterprise/userModify/v1',
                              'updateBy': '李杰',
                              'updateTime': '2019-08-28 11:25:08',
                              'uuid': '4fffd3a677fd42fab14fd28e6c1f6b6c'
                            },
                            {
                              'checked': true,
                              'children': [

                              ],
                              'createBy': '李杰',
                              'createTime': '2019-08-28 11:27:20',
                              'defaultexpandedKeys': [

                              ],
                              'delFlag': 0,
                              'disabled': false,
                              'disabledFlag': 0,
                              'hasAcl': true,
                              'id': 198063685430808576,
                              'idStr': '198063685430808576',
                              'label': '企业用户密码修改',
                              'remark': '',
                              'sysAclAction': 'userPwd',
                              'sysAclCode': '',
                              'sysAclComponentName': '',
                              'sysAclIcon': '',
                              'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705.198058969976147968',
                              'sysAclName': '企业用户密码修改',
                              'sysAclParentId': 198058969976147968,
                              'sysAclParentIdStr': '198058969976147968',
                              'sysAclPermissionCode': '1',
                              'sysAclRouter': 'SysEnterpriseUserList',
                              'sysAclSeq': 1,
                              'sysAclType': 2,
                              'sysAclUrl': '/api/platform/enterprise/sysUserPwd/v1',
                              'updateBy': '李杰',
                              'updateTime': '2019-08-28 11:31:47',
                              'uuid': '297c8af9b87d4ac6912630774fdbc3d6'
                            },
                            {
                              'checked': true,
                              'children': [

                              ],
                              'createBy': '李杰',
                              'createTime': '2019-08-28 11:28:39',
                              'defaultexpandedKeys': [

                              ],
                              'delFlag': 0,
                              'disabled': false,
                              'disabledFlag': 0,
                              'hasAcl': true,
                              'id': 198064017837789184,
                              'idStr': '198064017837789184',
                              'label': '企业用户授权角色(回显)',
                              'remark': '',
                              'sysAclAction': 'userCheckRoles',
                              'sysAclCode': '',
                              'sysAclComponentName': '',
                              'sysAclIcon': '',
                              'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705.198058969976147968',
                              'sysAclName': '企业用户授权角色(回显)',
                              'sysAclParentId': 198058969976147968,
                              'sysAclParentIdStr': '198058969976147968',
                              'sysAclPermissionCode': '1',
                              'sysAclRouter': 'SysEnterpriseUserList',
                              'sysAclSeq': 1,
                              'sysAclType': 2,
                              'sysAclUrl': '/api/platform/enterprise/userCheckRoles/v1',
                              'updateBy': '李杰',
                              'updateTime': '2019-08-28 11:28:39',
                              'uuid': '5562663686ee4c998c061ee692e4dc5e'
                            },
                            {
                              'checked': true,
                              'children': [

                              ],
                              'createBy': '李杰',
                              'createTime': '2019-08-28 11:28:56',
                              'defaultexpandedKeys': [

                              ],
                              'delFlag': 0,
                              'disabled': false,
                              'disabledFlag': 0,
                              'hasAcl': true,
                              'id': 198064089065459712,
                              'idStr': '198064089065459712',
                              'label': '企业用户授权角色(授权)',
                              'remark': '',
                              'sysAclAction': 'rolesToUser',
                              'sysAclCode': '',
                              'sysAclComponentName': '',
                              'sysAclIcon': '',
                              'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705.198058969976147968',
                              'sysAclName': '企业用户授权角色(授权)',
                              'sysAclParentId': 198058969976147968,
                              'sysAclParentIdStr': '198058969976147968',
                              'sysAclPermissionCode': '1',
                              'sysAclRouter': 'SysEnterpriseUserList',
                              'sysAclSeq': 1,
                              'sysAclType': 2,
                              'sysAclUrl': '/api/platform/enterprise/rolesToUser/v1',
                              'updateBy': '李杰',
                              'updateTime': '2019-08-28 11:31:36',
                              'uuid': 'e7a7882e3881490e9c3eaba155273284'
                            },
                            {
                              'checked': true,
                              'children': [

                              ],
                              'createBy': '李杰',
                              'createTime': '2019-08-28 11:29:39',
                              'defaultexpandedKeys': [

                              ],
                              'delFlag': 0,
                              'disabled': false,
                              'disabledFlag': 0,
                              'hasAcl': true,
                              'id': 198064269110153216,
                              'idStr': '198064269110153216',
                              'label': '企业用户状态',
                              'remark': '',
                              'sysAclAction': 'userStatus',
                              'sysAclCode': '',
                              'sysAclComponentName': '',
                              'sysAclIcon': '',
                              'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705.198058969976147968',
                              'sysAclName': '企业用户状态',
                              'sysAclParentId': 198058969976147968,
                              'sysAclParentIdStr': '198058969976147968',
                              'sysAclPermissionCode': '1',
                              'sysAclRouter': 'SysEnterpriseUserList',
                              'sysAclSeq': 1,
                              'sysAclType': 2,
                              'sysAclUrl': '/api/platform/enterprise/userStatus/v1',
                              'updateBy': '李杰',
                              'updateTime': '2019-08-28 11:29:39',
                              'uuid': 'de0b28094cf5492f99f4991cb7071c03'
                            }
                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-28 11:08:36',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 198058969976147968,
                          'idStr': '198058969976147968',
                          'label': '企业用户管理',
                          'remark': '',
                          'sysAclAction': 'SysEnterpriseUserList',
                          'sysAclCode': '',
                          'sysAclComponentName': 'SysEnterpriseUserList',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705',
                          'sysAclName': '企业用户管理',
                          'sysAclParentId': 195332810922397705,
                          'sysAclParentIdStr': '195332810922397705',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'SysEnterpriseUserList',
                          'sysAclSeq': 1,
                          'sysAclType': 1,
                          'sysAclUrl': '/api/platform/enterprise/userManager/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 11:22:52',
                          'uuid': '52130fdd357642488e0040277e9cfbe7'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-28 11:48:52',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 198069104119255040,
                          'idStr': '198069104119255040',
                          'label': '企业状态',
                          'remark': '',
                          'sysAclAction': 'status',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705',
                          'sysAclName': '企业状态',
                          'sysAclParentId': 195332810922397705,
                          'sysAclParentIdStr': '195332810922397705',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'SysEnterpriseList',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/enterprise/status/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 11:48:52',
                          'uuid': 'f12a0448208a4e84ba4748050b9b4f25'
                        },
                        {
                          'checked': true,
                          'children': [
                            {
                              'checked': true,
                              'children': [

                              ],
                              'createBy': '李杰',
                              'createTime': '2019-08-22 10:26:13',
                              'defaultexpandedKeys': [

                              ],
                              'delFlag': 0,
                              'disabled': false,
                              'disabledFlag': 0,
                              'hasAcl': true,
                              'id': 195873978903040000,
                              'idStr': '195873978903040000',
                              'label': '企业角色列表',
                              'remark': '',
                              'sysAclAction': 'listRole',
                              'sysAclCode': '',
                              'sysAclComponentName': '',
                              'sysAclIcon': '',
                              'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705.198088282079367168',
                              'sysAclName': '企业角色列表',
                              'sysAclParentId': 198088282079367168,
                              'sysAclParentIdStr': '198088282079367168',
                              'sysAclPermissionCode': '1',
                              'sysAclRouter': 'SysEnterpriseList',
                              'sysAclSeq': 1,
                              'sysAclType': 2,
                              'sysAclUrl': '/api/platform/enterprise/roleList/v1',
                              'updateBy': '李杰',
                              'updateTime': '2019-08-28 13:05:37',
                              'uuid': 'c4dd9b8b74154bfe85dedb7fa1d0da72'
                            },
                            {
                              'checked': true,
                              'children': [

                              ],
                              'createBy': '李杰',
                              'createTime': '2019-08-28 13:06:33',
                              'defaultexpandedKeys': [

                              ],
                              'delFlag': 0,
                              'disabled': false,
                              'disabledFlag': 0,
                              'hasAcl': true,
                              'id': 198088655322091520,
                              'idStr': '198088655322091520',
                              'label': '企业角色新增',
                              'remark': '',
                              'sysAclAction': 'add',
                              'sysAclCode': '',
                              'sysAclComponentName': '',
                              'sysAclIcon': '',
                              'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705.198088282079367168',
                              'sysAclName': '企业角色新增',
                              'sysAclParentId': 198088282079367168,
                              'sysAclParentIdStr': '198088282079367168',
                              'sysAclPermissionCode': '1',
                              'sysAclRouter': 'SysEnterpriseRoleList',
                              'sysAclSeq': 1,
                              'sysAclType': 2,
                              'sysAclUrl': '/api/platform/enterprise/roleAdd/v1',
                              'updateBy': '李杰',
                              'updateTime': '2019-08-28 13:06:33',
                              'uuid': 'f396973958a94b43971df535489d8e81'
                            },
                            {
                              'checked': true,
                              'children': [

                              ],
                              'createBy': '李杰',
                              'createTime': '2019-08-28 13:07:00',
                              'defaultexpandedKeys': [

                              ],
                              'delFlag': 0,
                              'disabled': false,
                              'disabledFlag': 0,
                              'hasAcl': true,
                              'id': 198088766655696896,
                              'idStr': '198088766655696896',
                              'label': '企业角色编辑',
                              'remark': '',
                              'sysAclAction': 'edit',
                              'sysAclCode': '',
                              'sysAclComponentName': '',
                              'sysAclIcon': '',
                              'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705.198088282079367168',
                              'sysAclName': '企业角色编辑',
                              'sysAclParentId': 198088282079367168,
                              'sysAclParentIdStr': '198088282079367168',
                              'sysAclPermissionCode': '1',
                              'sysAclRouter': 'SysEnterpriseRoleList',
                              'sysAclSeq': 1,
                              'sysAclType': 2,
                              'sysAclUrl': '/api/platform/enterprise/roleModify/v1',
                              'updateBy': '李杰',
                              'updateTime': '2019-08-28 13:07:00',
                              'uuid': 'cff2fa8bb01a4897b83e160f0d346f6c'
                            },
                            {
                              'checked': true,
                              'children': [

                              ],
                              'createBy': '李杰',
                              'createTime': '2019-08-28 13:07:51',
                              'defaultexpandedKeys': [

                              ],
                              'delFlag': 0,
                              'disabled': false,
                              'disabledFlag': 0,
                              'hasAcl': true,
                              'id': 198088981710245888,
                              'idStr': '198088981710245888',
                              'label': '企业角色授权(查看)',
                              'remark': '',
                              'sysAclAction': 'roleHaveAclTree',
                              'sysAclCode': '',
                              'sysAclComponentName': '',
                              'sysAclIcon': '',
                              'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705.198088282079367168',
                              'sysAclName': '企业角色授权(查看)',
                              'sysAclParentId': 198088282079367168,
                              'sysAclParentIdStr': '198088282079367168',
                              'sysAclPermissionCode': '1',
                              'sysAclRouter': 'SysEnterpriseRoleList',
                              'sysAclSeq': 1,
                              'sysAclType': 2,
                              'sysAclUrl': '/api/platform/enterprise/roleHaveAclTree/v1',
                              'updateBy': '李杰',
                              'updateTime': '2019-08-28 13:07:51',
                              'uuid': '69673415b9b549c98bbdd790d75b9da8'
                            },
                            {
                              'checked': true,
                              'children': [

                              ],
                              'createBy': '李杰',
                              'createTime': '2019-08-28 13:15:29',
                              'defaultexpandedKeys': [

                              ],
                              'delFlag': 0,
                              'disabled': false,
                              'disabledFlag': 0,
                              'hasAcl': true,
                              'id': 198090905197088768,
                              'idStr': '198090905197088768',
                              'label': '企业角色授权(授权)',
                              'remark': '',
                              'sysAclAction': 'aclsToRole',
                              'sysAclCode': '',
                              'sysAclComponentName': '',
                              'sysAclIcon': '',
                              'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705.198088282079367168',
                              'sysAclName': '企业角色授权(授权)',
                              'sysAclParentId': 198088282079367168,
                              'sysAclParentIdStr': '198088282079367168',
                              'sysAclPermissionCode': '1',
                              'sysAclRouter': 'SysEnterpriseRoleList',
                              'sysAclSeq': 1,
                              'sysAclType': 2,
                              'sysAclUrl': '/api/platform/enterprise/aclsToRole/v1',
                              'updateBy': '李杰',
                              'updateTime': '2019-08-28 13:15:29',
                              'uuid': '7f411f0a08ad46ca990f43cc6b852fcf'
                            },
                            {
                              'checked': true,
                              'children': [

                              ],
                              'createBy': '李杰',
                              'createTime': '2019-08-28 13:20:27',
                              'defaultexpandedKeys': [

                              ],
                              'delFlag': 0,
                              'disabled': false,
                              'disabledFlag': 0,
                              'hasAcl': true,
                              'id': 198092153824612352,
                              'idStr': '198092153824612352',
                              'label': '企业角色状态',
                              'remark': '',
                              'sysAclAction': 'roleStatus',
                              'sysAclCode': '',
                              'sysAclComponentName': '',
                              'sysAclIcon': '',
                              'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705.198088282079367168',
                              'sysAclName': '企业角色状态',
                              'sysAclParentId': 198088282079367168,
                              'sysAclParentIdStr': '198088282079367168',
                              'sysAclPermissionCode': '1',
                              'sysAclRouter': 'SysEnterpriseRoleList',
                              'sysAclSeq': 1,
                              'sysAclType': 2,
                              'sysAclUrl': '/api/platform/enterprise/roleStatus/v1',
                              'updateBy': '李杰',
                              'updateTime': '2019-08-28 13:20:27',
                              'uuid': 'd55579ea91964c9aa8c79e2caf5a16bb'
                            }
                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-28 13:05:04',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 198088282079367168,
                          'idStr': '198088282079367168',
                          'label': '企业角色管理',
                          'remark': '',
                          'sysAclAction': 'SysEnterpriseRoleList',
                          'sysAclCode': '',
                          'sysAclComponentName': 'SysEnterpriseRoleList',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397705',
                          'sysAclName': '企业角色管理',
                          'sysAclParentId': 195332810922397705,
                          'sysAclParentIdStr': '195332810922397705',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'SysEnterpriseRoleList',
                          'sysAclSeq': 1,
                          'sysAclType': 1,
                          'sysAclUrl': '/api/platform/enterprise/roleManager/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 13:05:04',
                          'uuid': '303d4540ccd74c5c9daf6e0cac587564'
                        }
                      ],
                      'createBy': '',
                      'createTime': '2019-08-02 15:10:29',
                      'defaultexpandedKeys': [

                      ],
                      'delFlag': 0,
                      'disabled': false,
                      'disabledFlag': 0,
                      'hasAcl': true,
                      'id': 195332810922397705,
                      'idStr': '195332810922397705',
                      'label': '企业管理',
                      'remark': '',
                      'sysAclAction': 'manager',
                      'sysAclCode': '',
                      'sysAclComponentName': 'SysEnterpriseUserList',
                      'sysAclIcon': '',
                      'sysAclLevel': '0.195332810922397711.195332810922397710',
                      'sysAclName': '企业管理',
                      'sysAclParentId': 195332810922397710,
                      'sysAclParentIdStr': '195332810922397710',
                      'sysAclPermissionCode': '1',
                      'sysAclRouter': 'SysEnterpriseUserList',
                      'sysAclSeq': 1,
                      'sysAclType': 0,
                      'sysAclUrl': '188696496605106238',
                      'updateBy': '李杰',
                      'updateTime': '2019-08-28 14:09:35',
                      'uuid': '188696496605106238'
                    },
                    {
                      'checked': true,
                      'children': [

                      ],
                      'createBy': '',
                      'createTime': '2019-08-02 15:09:53',
                      'defaultexpandedKeys': [

                      ],
                      'delFlag': 0,
                      'disabled': false,
                      'disabledFlag': 0,
                      'hasAcl': true,
                      'id': 195332810922397706,
                      'idStr': '195332810922397706',
                      'label': '部门管理',
                      'remark': '',
                      'sysAclAction': 'manager',
                      'sysAclCode': '',
                      'sysAclComponentName': '',
                      'sysAclIcon': '',
                      'sysAclLevel': '0.195332810922397711.195332810922397710',
                      'sysAclName': '部门管理',
                      'sysAclParentId': 195332810922397710,
                      'sysAclParentIdStr': '195332810922397710',
                      'sysAclPermissionCode': '1',
                      'sysAclRouter': '',
                      'sysAclSeq': 1,
                      'sysAclType': 0,
                      'sysAclUrl': '188696496605106239',
                      'updateBy': '李杰',
                      'updateTime': '2019-08-21 23:01:21',
                      'uuid': 'afae14c9a9d843c6b865872abd75955f'
                    },
                    {
                      'checked': true,
                      'children': [
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '',
                          'createTime': '2019-04-13 11:30:28',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195332810905620488,
                          'idStr': '195332810905620488',
                          'label': '角色列表',
                          'remark': '',
                          'sysAclAction': 'list',
                          'sysAclCode': '',
                          'sysAclComponentName': 'AuthorRole',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397707',
                          'sysAclName': '角色列表',
                          'sysAclParentId': 195332810922397707,
                          'sysAclParentIdStr': '195332810922397707',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AuthorRole',
                          'sysAclSeq': 1,
                          'sysAclType': 1,
                          'sysAclUrl': '/api/platform/role/roleList/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 09:59:00',
                          'uuid': '2c0208809d974940b284ce4522d3e4dd'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 10:13:46',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195870845300772864,
                          'idStr': '195870845300772864',
                          'label': '新增',
                          'remark': '',
                          'sysAclAction': 'add',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397707',
                          'sysAclName': '新增',
                          'sysAclParentId': 195332810922397707,
                          'sysAclParentIdStr': '195332810922397707',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AuthorRole',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/role/save/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 09:59:15',
                          'uuid': 'b30d24305f1f4def86bb8d3657b7fa4d'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 10:15:31',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195871284125634560,
                          'idStr': '195871284125634560',
                          'label': '编辑',
                          'remark': '',
                          'sysAclAction': 'edit',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397707',
                          'sysAclName': '编辑',
                          'sysAclParentId': 195332810922397707,
                          'sysAclParentIdStr': '195332810922397707',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AuthorRole',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/role/modify/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 09:59:19',
                          'uuid': 'c893cb23b0b34c44b7f88888d289b936'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 10:15:52',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195871372877107200,
                          'idStr': '195871372877107200',
                          'label': '变更角色状态',
                          'remark': '',
                          'sysAclAction': 'status',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397707',
                          'sysAclName': '变更角色状态',
                          'sysAclParentId': 195332810922397707,
                          'sysAclParentIdStr': '195332810922397707',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AuthorRole',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/role/status/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 09:59:23',
                          'uuid': '7ba96f13f8bc4848b66f6ceca94c345e'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-27 18:39:46',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 197810122012102656,
                          'idStr': '197810122012102656',
                          'label': '权限给角色(回显)',
                          'remark': '',
                          'sysAclAction': 'aclShow',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397707',
                          'sysAclName': '权限给角色(回显)',
                          'sysAclParentId': 195332810922397707,
                          'sysAclParentIdStr': '195332810922397707',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AuthorRole',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/role/roleHaveAclTree/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 09:59:27',
                          'uuid': '8d5b6d89e1204305b70b3a455270e17f'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-27 18:41:49',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 197810639492747264,
                          'idStr': '197810639492747264',
                          'label': '权限给角色(授权)',
                          'remark': '',
                          'sysAclAction': 'aclAuthor',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397707',
                          'sysAclName': '权限给角色(授权)',
                          'sysAclParentId': 195332810922397707,
                          'sysAclParentIdStr': '195332810922397707',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AuthorRole',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/role/aclsToRole/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 09:59:31',
                          'uuid': '2ff2433314254f2b8b0c03ea356c7948'
                        }
                      ],
                      'createBy': '',
                      'createTime': '2019-08-02 15:09:04',
                      'defaultexpandedKeys': [

                      ],
                      'delFlag': 0,
                      'disabled': false,
                      'disabledFlag': 0,
                      'hasAcl': true,
                      'id': 195332810922397707,
                      'idStr': '195332810922397707',
                      'label': '角色管理',
                      'remark': '',
                      'sysAclAction': 'RoleManager',
                      'sysAclCode': '',
                      'sysAclComponentName': '',
                      'sysAclIcon': '',
                      'sysAclLevel': '0.195332810922397711.195332810922397710',
                      'sysAclName': '角色管理',
                      'sysAclParentId': 195332810922397710,
                      'sysAclParentIdStr': '195332810922397710',
                      'sysAclPermissionCode': '1',
                      'sysAclRouter': 'AuthorRole',
                      'sysAclSeq': 1,
                      'sysAclType': 0,
                      'sysAclUrl': '/api/platform/role/catalogue/v1',
                      'updateBy': '李杰',
                      'updateTime': '2019-08-28 14:09:07',
                      'uuid': 'f2d1ea1a116343b6ba3910d3734cbe72'
                    },
                    {
                      'checked': true,
                      'children': [
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '',
                          'createTime': '2019-04-13 11:26:58',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195332810905620484,
                          'idStr': '195332810905620484',
                          'label': '菜单列表',
                          'remark': '',
                          'sysAclAction': 'list',
                          'sysAclCode': '',
                          'sysAclComponentName': 'AclTree',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397708',
                          'sysAclName': '菜单列表',
                          'sysAclParentId': 195332810922397708,
                          'sysAclParentIdStr': '195332810922397708',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AclTree',
                          'sysAclSeq': 1,
                          'sysAclType': 1,
                          'sysAclUrl': '/api/platform/acl/aclTree/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-22 10:29:39',
                          'uuid': '5f2056a6a51e441bb904dc6709a817e4'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 10:08:15',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195869456721907712,
                          'idStr': '195869456721907712',
                          'label': '新增权限系统',
                          'remark': '',
                          'sysAclAction': 'moduleAdd',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397708',
                          'sysAclName': '新增权限系统',
                          'sysAclParentId': 195332810922397708,
                          'sysAclParentIdStr': '195332810922397708',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AclTree',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/acl/saveSys/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-22 10:08:15',
                          'uuid': '3d6d98cd8f12482896a6371247fb212e'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 10:09:07',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195869677409406976,
                          'idStr': '195869677409406976',
                          'label': '编辑',
                          'remark': '',
                          'sysAclAction': 'edit',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397708',
                          'sysAclName': '编辑',
                          'sysAclParentId': 195332810922397708,
                          'sysAclParentIdStr': '195332810922397708',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AclTree',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/acl/modify/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-22 10:09:07',
                          'uuid': '77aaeeccdcc447d1848017c45d960735'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 10:09:31',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195869776344649728,
                          'idStr': '195869776344649728',
                          'label': '查询详情',
                          'remark': '',
                          'sysAclAction': 'detail',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397708',
                          'sysAclName': '查询详情',
                          'sysAclParentId': 195332810922397708,
                          'sysAclParentIdStr': '195332810922397708',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AclTree',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/acl/detail/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-22 10:09:31',
                          'uuid': 'ef2b4f4b97cd4ad58ee83899ab36d19d'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 10:10:03',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195869910314913792,
                          'idStr': '195869910314913792',
                          'label': '新增',
                          'remark': '',
                          'sysAclAction': 'add',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397708',
                          'sysAclName': '新增',
                          'sysAclParentId': 195332810922397708,
                          'sysAclParentIdStr': '195332810922397708',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AclTree',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/acl/save/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-22 10:10:03',
                          'uuid': '8670234a1f984917ae448df41d0101e1'
                        }
                      ],
                      'createBy': '',
                      'createTime': '2019-08-02 15:08:01',
                      'defaultexpandedKeys': [

                      ],
                      'delFlag': 0,
                      'disabled': false,
                      'disabledFlag': 0,
                      'hasAcl': true,
                      'id': 195332810922397708,
                      'idStr': '195332810922397708',
                      'label': '菜单管理',
                      'remark': '',
                      'sysAclAction': 'AclTreeManager',
                      'sysAclCode': '',
                      'sysAclComponentName': '',
                      'sysAclIcon': '',
                      'sysAclLevel': '0.195332810922397711.195332810922397710',
                      'sysAclName': '菜单管理',
                      'sysAclParentId': 195332810922397710,
                      'sysAclParentIdStr': '195332810922397710',
                      'sysAclPermissionCode': '1',
                      'sysAclRouter': 'AclTree',
                      'sysAclSeq': 1,
                      'sysAclType': 0,
                      'sysAclUrl': '/api/platform/acl/catalogue/v1',
                      'updateBy': '李杰',
                      'updateTime': '2019-08-28 14:09:13',
                      'uuid': 'b593168f64724ef2aadb55e90bf2fe53'
                    },
                    {
                      'checked': true,
                      'children': [
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-04-13 11:22:15',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195332810905620480,
                          'idStr': '195332810905620480',
                          'label': '用户列表',
                          'remark': '',
                          'sysAclAction': 'list',
                          'sysAclCode': '',
                          'sysAclComponentName': 'AuthorUser',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397709',
                          'sysAclName': '用户列表',
                          'sysAclParentId': 195332810922397709,
                          'sysAclParentIdStr': '195332810922397709',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AuthorUser',
                          'sysAclSeq': 1,
                          'sysAclType': 1,
                          'sysAclUrl': '/api/platform/sysUser/sysUserList/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-22 10:29:16',
                          'uuid': 'c70a20be0a2e4a1889af5d69263dc972'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 09:55:55',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195866352387493888,
                          'idStr': '195866352387493888',
                          'label': '新增',
                          'remark': '',
                          'sysAclAction': 'add',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397709',
                          'sysAclName': '新增',
                          'sysAclParentId': 195332810922397709,
                          'sysAclParentIdStr': '195332810922397709',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AuthorUser',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/sysUser/sysUserAdd/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 09:58:08',
                          'uuid': '5d166d20fb6945a786dc68e9db7bd706'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 09:56:45',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195866564606693376,
                          'idStr': '195866564606693376',
                          'label': '编辑',
                          'remark': '',
                          'sysAclAction': 'edit',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397709',
                          'sysAclName': '编辑',
                          'sysAclParentId': 195332810922397709,
                          'sysAclParentIdStr': '195332810922397709',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AuthorUser',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/sysUser/sysUserModify/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 09:58:16',
                          'uuid': '149ffb4d4a734a4a891cfbab5ff0155e'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 09:57:31',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195866757108469760,
                          'idStr': '195866757108469760',
                          'label': '修改密码',
                          'remark': '',
                          'sysAclAction': 'editPwd',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397709',
                          'sysAclName': '修改密码',
                          'sysAclParentId': 195332810922397709,
                          'sysAclParentIdStr': '195332810922397709',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AuthorUser',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/sysUser/sysUserPwd/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 09:58:22',
                          'uuid': '6e718ba386dd4da2bc8650b115203b0b'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 09:59:17',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195867199867588608,
                          'idStr': '195867199867588608',
                          'label': '角色给用户(回显)',
                          'remark': '',
                          'sysAclAction': 'roleShow',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397709',
                          'sysAclName': '角色给用户(回显)',
                          'sysAclParentId': 195332810922397709,
                          'sysAclParentIdStr': '195332810922397709',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AuthorUser',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/sysUser/userCheckRoles/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 09:58:27',
                          'uuid': '651de34888fe4f8e870c4b66ad5f3259'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 09:59:45',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195867319661105152,
                          'idStr': '195867319661105152',
                          'label': '角色给用户(授权)',
                          'remark': '',
                          'sysAclAction': 'roleAuthor',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397709',
                          'sysAclName': '角色给用户(授权)',
                          'sysAclParentId': 195332810922397709,
                          'sysAclParentIdStr': '195332810922397709',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AuthorUser',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/sysUser/rolesToUser/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 09:58:32',
                          'uuid': 'a0218f58b2854b84bd54608bef95453d'
                        },
                        {
                          'checked': true,
                          'children': [

                          ],
                          'createBy': '李杰',
                          'createTime': '2019-08-22 10:00:23',
                          'defaultexpandedKeys': [

                          ],
                          'delFlag': 0,
                          'disabled': false,
                          'disabledFlag': 0,
                          'hasAcl': true,
                          'id': 195867479266955264,
                          'idStr': '195867479266955264',
                          'label': '状态',
                          'remark': '',
                          'sysAclAction': 'status',
                          'sysAclCode': '',
                          'sysAclComponentName': '',
                          'sysAclIcon': '',
                          'sysAclLevel': '0.195332810922397711.195332810922397710.195332810922397709',
                          'sysAclName': '状态',
                          'sysAclParentId': 195332810922397709,
                          'sysAclParentIdStr': '195332810922397709',
                          'sysAclPermissionCode': '1',
                          'sysAclRouter': 'AuthorUser',
                          'sysAclSeq': 1,
                          'sysAclType': 2,
                          'sysAclUrl': '/api/platform/sysUser/sysUserStatus/v1',
                          'updateBy': '李杰',
                          'updateTime': '2019-08-28 09:58:39',
                          'uuid': 'cea452c89a77455ea4ed743a5b37a014'
                        }
                      ],
                      'createBy': '',
                      'createTime': '2019-08-02 15:07:44',
                      'defaultexpandedKeys': [

                      ],
                      'delFlag': 0,
                      'disabled': false,
                      'disabledFlag': 0,
                      'hasAcl': true,
                      'id': 195332810922397709,
                      'idStr': '195332810922397709',
                      'label': '用户管理',
                      'remark': '',
                      'sysAclAction': 'UserManager',
                      'sysAclCode': '',
                      'sysAclComponentName': '',
                      'sysAclIcon': '',
                      'sysAclLevel': '0.195332810922397711.195332810922397710',
                      'sysAclName': '用户管理',
                      'sysAclParentId': 195332810922397710,
                      'sysAclParentIdStr': '195332810922397710',
                      'sysAclPermissionCode': '1',
                      'sysAclRouter': 'AuthorUser',
                      'sysAclSeq': 1,
                      'sysAclType': 0,
                      'sysAclUrl': '/api/platform/sysUser/catalogue/v1',
                      'updateBy': '李杰',
                      'updateTime': '2019-08-28 14:09:20',
                      'uuid': 'e88c27d65f4a4e75bd5842175bfd6371'
                    }
                  ],
                  'createBy': '',
                  'createTime': '2019-08-02 15:07:11',
                  'defaultexpandedKeys': [

                  ],
                  'delFlag': 0,
                  'disabled': false,
                  'disabledFlag': 0,
                  'hasAcl': true,
                  'id': 195332810922397710,
                  'idStr': '195332810922397710',
                  'label': '权限管理',
                  'remark': '',
                  'sysAclAction': 'manager',
                  'sysAclCode': '',
                  'sysAclComponentName': 'Auth',
                  'sysAclIcon': '',
                  'sysAclLevel': '0.195332810922397711',
                  'sysAclName': '权限管理',
                  'sysAclParentId': 195332810922397711,
                  'sysAclParentIdStr': '195332810922397711',
                  'sysAclPermissionCode': '1',
                  'sysAclRouter': 'Auth',
                  'sysAclSeq': 1,
                  'sysAclType': 0,
                  'sysAclUrl': '61fcd23e1aad4a2581160de1fce545a7',
                  'updateBy': '李杰',
                  'updateTime': '2019-08-21 23:01:31',
                  'uuid': '61fcd23e1aad4a2581160de1fce545a7'
                }
              ],
              'createBy': '',
              'createTime': '2019-08-02 15:04:14',
              'defaultexpandedKeys': [

              ],
              'delFlag': 0,
              'disabled': false,
              'disabledFlag': 0,
              'hasAcl': true,
              'id': 195332810922397711,
              'idStr': '195332810922397711',
              'label': '后台管理模块',
              'remark': '',
              'sysAclAction': 'manager',
              'sysAclCode': '',
              'sysAclComponentName': '',
              'sysAclIcon': '',
              'sysAclLevel': '0',
              'sysAclName': '后台管理模块',
              'sysAclParentId': 0,
              'sysAclParentIdStr': '0',
              'sysAclPermissionCode': '1',
              'sysAclRouter': '',
              'sysAclSeq': 1,
              'sysAclType': -1,
              'sysAclUrl': '188696496605106244',
              'updateBy': '',
              'updateTime': '2019-08-02 15:04:16',
              'uuid': '61fcd23e1aad4a2581160de1fce545a6'
            }
          ]
        },
        'msg': 'OK',
        'oK': true,
        'ok': '',
        'status': 200
      }
    }
  },

  // get user info
  {
    url: '/user/info\.*',
    type: 'get',
    response: config => {
      const { token } = config.query
      const info = users[token]

      // mock error
      if (!info) {
        return {
          code: 50008,
          message: 'Login failed, unable to get user details.'
        }
      }

      return {
        code: 20000,
        data: info
      }
    }
  },

  // user logout
  {
    url: '/platform/sysMain/logout/v1',
    type: 'post',
    response: _ => {
      return {
        data: '安全退出成功',
        msg: '5cc9a0b69dfa4db6b0f87c9e45f5713b',
        oK: true,
        status: 200
      }
    }
  }
]
