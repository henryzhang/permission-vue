/**
 * axios封装
 * 请求拦截、响应拦截、错误统一处理
 * https://www.miaoroom.com/code/ued/vue-axios.html
 */
// elementui主要样式
// elemnetui边界响应样式
import 'element-ui/lib/theme-chalk/display.css'
import axios from 'axios'
import router from '@/router/index'
import { getToken, setToken, removeToken } from '@/utils/auth'
import store from '../store/index'
import { warnin, error } from '@/utils/tip'

/**
 * 跳转登录页
 * 携带当前页面路由，以期在登录页面完成登录后返回当前页面
 */
const toLogin = () => {
  router.replace({
    path: '/',
    query: {
      redirect: router.currentRoute.fullPath
    }
  })
}

/**
 * 请求失败后的错误统一处理
 * @param {Number} status 请求失败的状态码
 */
const errorHandle = (res) => {
  const status = res.status
  // 状态码判断
  switch (status) {
    // 401: 未登录状态，跳转登录页
    case 401:
      warnin(res.data)
      setTimeout(() => {
        store.dispatch('user/resetToken').then(() => {
          location.reload()
        })
      }, 2000)
      return Promise.reject(res)
    // 403 token过期
    // 清除token并跳转登录页
    case 403:
      localStorage.removeItem('token')
      // store.commit('loginSuccess', null)
      setTimeout(() => {
        toLogin()
      }, 1000)
      return Promise.reject(res)
    // 404请求不存在
    case 404:
      warnin('请求不存在')
      return Promise.reject(res)
    default:
      error(res.data)
      return Promise.reject(res)
  }
}

window.axiosCancel = [] // 全局定义一个存放取消请求的标识
// 创建axios实例
var instance = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  timeout: 12000,
  withCredentials: false // send cookies when cross-domain requests
})
// 设置post请求头
instance.defaults.headers.post['Content-Type'] = 'application/json;charset=UTF-8'
/**
 * 请求拦截器
 * 每次请求前，如果存在token则在请求头中携带token
 */
instance.interceptors.request.use(
  config => {
    // 登录流程控制中，根据本地是否存在token判断用户的登录情况
    // 但是即使token存在，也有可能token是过期的，所以在每次的请求头中携带token
    // 后台根据携带的token判断用户的登录情况，并返回给我们对应的状态码
    // 而后我们可以在响应拦截器中，根据状态码进行一些统一的操作。
    const token = getToken()
    token && (config.headers.Authorization = token)
    config.headers['X-Token'] = token
    return config
  },
  error => Promise.error(error))

// 响应拦截器
instance.interceptors.response.use(
  // 请求成功
  res => {
    if (res.data.status === 200) {
      return Promise.resolve(res.data)
    } else {
      if (res.data.status === -1) {
        errorHandle(res.data)
        return Promise.resolve(res.data)
      }
      return errorHandle(res.data)
    }
    // return res.data.status === 200 ? Promise.resolve(res) : Promise.reject(res)
  },
  // 请求失败
  error => {
    const { response } = error
    if (response) {
      // 请求已发出，但是不在2xx的范围
      errorHandle(response, response.data.message)
      return Promise.reject(response)
    } else {
      // 处理断网的情况
      // eg:请求超时或断网时，更新state的network状态
      // network状态在app.vue中控制着一个全局的断网提示组件的显示隐藏
      // 关于断网组件中的刷新重新获取数据，会在断网组件中说明
      // store.commit('changeNetwork', false)
      warnin('请求的资源不存在')
    }
  })

export default instance
