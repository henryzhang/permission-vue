import request from '@/utils/request'

// 企业用户重置密码
export function sysUserPwd(data) {
  return request({
    url: '/platform/enterprise/sysUserPwd/v1',
    method: 'post',
    data
  })
}
// 用户状态设置
export function userStatus(data) {
  return request({
    url: '/platform/enterprise/userStatus/v1',
    method: 'post',
    data
  })
}
// 企业用户授权角色(授权)
export function rolesToUser(data) {
  return request({
    url: '/platform/enterprise/rolesToUser/v1',
    method: 'post',
    data
  })
}
// 企业用户授权角色(回显)
export function userCheckRoles(data) {
  return request({
    url: '/platform/enterprise/userCheckRoles/v1',
    method: 'post',
    data
  })
}
// 企业用户编辑
export function userModify(data) {
  return request({
    url: '/platform/enterprise/userModify/v1',
    method: 'post',
    data
  })
}
// 企业用户详情
export function userDetail(data) {
  return request({
    url: '/platform/enterprise/userDetail/v1',
    method: 'post',
    data
  })
}
// 新增企业用户
export function userAdd(data) {
  return request({
    url: '/platform/enterprise/userAdd/v1',
    method: 'post',
    data
  })
}
// 企业用户列表
export function userList(data) {
  return request({
    url: '/platform/enterprise/userList/v1',
    method: 'post',
    data
  })
}
// 设置角色状态
export function roleStatus(data) {
  return request({
    url: '/platform/enterprise/roleStatus/v1',
    method: 'post',
    data
  })
}
// 企业权限给角色(授权)
export function aclsToRole(data) {
  return request({
    url: '/platform/enterprise/aclsToRole/v1',
    method: 'post',
    data
  })
}
// 企业权限给角色(回显)
export function roleHaveAclTree(data) {
  return request({
    url: '/platform/enterprise/roleHaveAclTree/v1',
    method: 'post',
    data
  })
}
// 企业角色编辑
export function roleModify(data) {
  return request({
    url: '/platform/enterprise/roleModify/v1',
    method: 'post',
    data
  })
}
// 企业角色详情
export function roleDetail(data) {
  return request({
    url: '/platform/enterprise/roleDetail/v1',
    method: 'post',
    data
  })
}
// 企业角色新增
export function roleAdd(data) {
  return request({
    url: '/platform/enterprise/roleAdd/v1',
    method: 'post',
    data
  })
}
// 企业角色列表
export function roleList(data) {
  return request({
    url: '/platform/enterprise/roleList/v1',
    method: 'post',
    data
  })
}
// 为企业授权权限
export function aclsToEnterprise(data) {
  return request({
    url: '/platform/enterprise/aclsToEnterprise/v1',
    method: 'post',
    data
  })
}
// 企业状态
export function status(data) {
  return request({
    url: '/platform/enterprise/status/v1',
    method: 'post',
    data
  })
}
// 企业编辑
export function modify(data) {
  return request({
    url: '/platform/enterprise/modify/v1',
    method: 'post',
    data
  })
}
// 企业新增
export function save(data) {
  return request({
    url: '/platform/enterprise/save/v1',
    method: 'post',
    data
  })
}
// 企业拥有的权限
export function aclDetail(data) {
  return request({
    url: '/platform/enterprise/aclDetail/v1',
    method: 'post',
    data
  })
}
// 企业详情
export function detail(data) {
  return request({
    url: '/platform/enterprise/detail/v1',
    method: 'post',
    data
  })
}
// 企业列表
export function sysUserGroupPage(data) {
  return request({
    url: '/platform/enterprise/sysUserGroupPage/v1',
    method: 'post',
    data
  })
}

