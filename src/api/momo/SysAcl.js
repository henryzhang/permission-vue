import request from '@/utils/request'

// 一键同步权限到Redis
export function aclsToRedis(data) {
  return request({
    url: '/platform/acl/aclsToRedis/v1',
    method: 'post',
    data
  })
}
// 编辑权限菜单
export function modify(data) {
  return request({
    url: '/platform/acl/modify/v1',
    method: 'post',
    data
  })
}
// 新增权限菜单
export function save(data) {
  return request({
    url: '/platform/acl/save/v1',
    method: 'post',
    data
  })
}
// 查询菜单权限详情
export function detail(data) {
  return request({
    url: '/platform/acl/detail/v1',
    method: 'post',
    data
  })
}
// 新增系统权限
export function saveSys(data) {
  return request({
    url: '/platform/acl/saveSys/v1',
    method: 'post',
    data
  })
}
// 菜单树
export function roleHaveAclTree(data) {
  return request({
    url: '/platform/acl/aclTree/v1',
    method: 'post',
    data
  })
}

