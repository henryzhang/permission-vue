import request from '@/utils/request'

// 获取验证码
export function captcha(data) {
  return request({
    url: '/platform/sysMain/images/captcha/v1',
    method: 'post',
    data
  })
}
// 用户退出
export function logout(data) {
  return request({
    url: '/platform/sysMain/logout/v1',
    method: 'post',
    data
  })
}
// 登录
export function userLogin(data) {
  return request({
    url: '/platform/sysMain/login/v1',
    method: 'post',
    data
  })
}
// 动态权限菜单
export function dynamicMenu(data) {
  return request({
    url: '/platform/authority/dynamicMenu/v1',
    method: 'post',
    data
  })
}
