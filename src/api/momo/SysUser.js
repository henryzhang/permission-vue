import request from '@/utils/request'

// 用户修改密码
export function sysUserPwd(data) {
  return request({
    url: '/platform/sysUser/sysUserPwd/v1',
    method: 'post',
    data
  })
}
// 角色给用户(授权)
export function rolesToUser(data) {
  return request({
    url: '/platform/role/rolesToUser/v1',
    method: 'post',
    data
  })
}
// 角色给用户(回显)
export function userCheckRoles(data) {
  return request({
    url: '/platform/role/userCheckRoles/v1',
    method: 'post',
    data
  })
}
// 变更用户状态
export function sysUserStatus(data) {
  return request({
    url: '/platform/sysUser/sysUserStatus/v1',
    method: 'post',
    data
  })
}
// 用户新增--账号密码
export function sysUserAdd(data) {
  return request({
    url: '/platform/sysUser/sysUserAdd/v1',
    method: 'post',
    data
  })
}
// 用户编辑
export function sysUserModify(data) {
  return request({
    url: '/platform/sysUser/sysUserModify/v1',
    method: 'post',
    data
  })
}
// uuid 查询用户详情
export function sysUserDetail(data) {
  return request({
    url: '/platform/sysUser/sysUserDetail/v1',
    method: 'post',
    data
  })
}

// 用户列表
export function sysUserList(data) {
  return request({
    url: '/platform/sysUser/sysUserList/v1',
    method: 'post',
    data
  })
}

