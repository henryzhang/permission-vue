
const mutations = {
  getSocketData(state, data) {
    console.log(data)
    if (data.action !== 'ping') {
      state.getSocketData.forEach((item, index) => {
        if (item.action === data.action) {
          state.getSocketData.splice(index, 1)
          return
        }
      })
      state.getSocketData.push(data)
    }
  }
}

const actions = {
  // 触发websocket存储事件
  getSocketDataAction({
    commit,
    dispatch }, data) {
    commit('getSocketData', data)
    console.log(data)
    // switch中根据收到不同的socket ation头去执行不同的保存
    switch (data.action) {
      case 'init':
        dispatch('initReturnCallback', data)
        break
    }
  }
}

export default {
  namespaced: true,
  mutations,
  actions
}
